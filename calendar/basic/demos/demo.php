<!DOCTYPE html>
<html>
<head>
<title>Calendar</title>

<style type="text/css">
	*		{margin:0;padding:0;list-style-type:none;}
	a,img		{border:0;}
	body		{font:12px/180% Arial, Helvetica, sans-serif, "Arial";}
	.container	{width:940px;margin:0 auto;}
</style>
<link rel="stylesheet" href="css/fullcalendar.css">
<link rel="stylesheet" href="css/fullcalendar.print.css" media='print'>
<link rel="stylesheet" type="text/css" href="jquery.datetimepicker.css"/>


</head>
<body>
	<hr>
	<p id='log'>
	<!--The log and reg part-->
	
	<input type="text" id="username" placeholder="Username" />
	<input type="password" id="password" placeholder="Password" />
	<button id="login_btn">Log In</button>
	<script type="text/javascript" src="ajax.js"></script>
	<button id="reg_btn">Sign In</button>
	<script type="text/javascript" src="bjax.js"></script>
        </p>

	<hr>
<button id="fresh">Fresh</button>
<script type="text/javascript">
	$("button#fresh").click(function(){
		$('#calendar').fullCalendar('updateEvent', event);
	})
	
</script>
<script type="text/javascript">
	$('#calendar').fullCalendar({
    dayClick: function(date, jsEvent, view) {

        alert('Clicked on: ' + date.format());

        alert('Coordinates: ' + jsEvent.pageX + ',' + jsEvent.pageY);

        alert('Current view: ' + view.name);

        // change the day's background color just for fun
        $(this).css('background-color', 'red');

    }
});
</script>
<div class="container">
	<div class="content">
		<div class="row-fluid">
			<div class="span12">
				<div class="box">
					<div class="box-head">
						<h3>Calendar</h3>
					</div>
					<div class="box-content box-nomargin">
						<div class="calendar"></div>
					</div>
				</div>
			</div>
		</div>
	</div>	
</div>

<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/fullcalendar.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="jquery.datetimepicker.js"></script>
<br><br>
<script type="text/javascript">
	$(document).ready(function(){

		$("div#editdiv").hide();
		$("div#adddiv").hide();
		$("div#l").hide();
	});
</script>
<div id="editdiv">
<hr><br><br>
		<h1>Edit OR Delete</h1>
		<div style="width:960px;margin:0 auto;">
		<h3>Date</h3>
		<input type="text" id="date"/><br><br>
		</div>
		<div style="width:960px;margin:0 auto;">
		<h3>Start time</h3>
		<input type="text" id="time1"/><br><br>
		</div>	
		<div style="width:960px;margin:0 auto;">
		<h3>End time</h3>
		<input type="text" id="time2"/><br><br>
		</div>
		<div style="width:960px;margin:0 auto;">
		<h3>Title</h3>
		<input type="text" id="title"/><br><br>
		</div>
		<div style="width:960px;margin:0 auto;">
		<button id="edit">Edit</button>
		<script type="text/javascript" src="edit.js"></script><br><br>
		</div>
		<div style="width:960px;margin:0 auto;">
		<button id="delete">Delete</button>
		<script type="text/javascript" src="delete.js"></script><br><br>
		</div>
	</div>
<div id="adddiv"><hr><br><br>
	<h1>Add event</h1>
	<div style="width:960px;margin:0 auto;">
	<h3>Date</h3>
	<input type="text" id="datetimepicker2"/><br><br>
	</div>
	<div style="width:960px;margin:0 auto;">
	<h3>Start time</h3>
	<input type="text" id="datetimepicker1"/><br><br>
	</div>	
	<div style="width:960px;margin:0 auto;">
	<h3>End time</h3>
	<input type="text" id="datetimepicker11"/><br><br>
	</div>
	<div style="width:960px;margin:0 auto;">
	<h3>Title</h3>
	<input type="text" id="addtitle"/><br><br>
	</div>
	<div style="width:960px;margin:0 auto;">
	<button id="add">Add</button>
	<script type="text/javascript" src="add.js"></script><br><br>
	</div>
	</div>
<script type="text/javascript">
$('#datetimepicker1').datetimepicker({
	value: '00:00',
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker11').datetimepicker({
	value: '00:00',
	datepicker:false,
	format:'H:i',
	step:5
});
$('#datetimepicker2').datetimepicker({
	value:'2014/10/27',
	yearOffset:0,
	lang:'en',
	timepicker:false,
	format:'d/m/Y',
	formatDate:'Y/m/d',
	//minDate:'-1970/01/02', // yesterday is minimum date
	//maxDate:'+1970/01/02' // and tommorow is maximum date calendar
});
</script>


<div id="l"><hr><br><br><button id="logoff_btn">Sign Out</button>
	<script type="text/javascript" src="cjax.js"></script><br><br></div>


</body>
</html>